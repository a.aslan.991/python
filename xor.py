from numpy import sum, array, random, dot, tanh;

def tanh_derivative(x):
    return 1 - x*x;

inputs = array([[0, 0], [0, 1], [1, 0], [1, 1]]);
outputs = array([[0], [1], [1], [0]]);
epochs = 10000;
lr = 0.1;

# Random weights and bias initialization
hidden_weights = random.uniform(size=(2, 2));
hidden_bias = random.uniform(size=(1, 2));
output_weights = random.uniform(size=(2, 1));
output_bias = random.uniform(size=(1, 1));

# Training algorithm
for _ in range(epochs):

    # Forward Propagation
    hidden_layer_activation = dot(inputs, hidden_weights);
    hidden_layer_activation += hidden_bias;
    hidden_layer_output = tanh(hidden_layer_activation);

    output_layer_activation = dot(hidden_layer_output, output_weights);
    output_layer_activation += output_bias;
    predicted_outputs = tanh(output_layer_activation);

    # Backpropagation
    error = outputs - predicted_outputs;
    d_predicted_output = error * tanh_derivative(predicted_outputs);

    error_hidden_layer = d_predicted_output.dot(output_weights.T);
    d_hidden_layer = error_hidden_layer * tanh_derivative(hidden_layer_output);

    # Updating Weights and Biases
    output_weights += hidden_layer_output.T.dot(d_predicted_output) * lr;
    output_bias += sum(d_predicted_output, axis=0, keepdims=True) * lr;
    hidden_weights += inputs.T.dot(d_hidden_layer) * lr;
    hidden_bias += sum(d_hidden_layer, axis=0, keepdims=True) * lr;

print("Final hidden weights: ", end='')
print(*hidden_weights)
print("Final hidden bias: ", end='')
print(*hidden_bias)
print("Final output weights: ", end='')
print(*output_weights)
print("Final output bias: ", end='')
print(*output_bias)

print("\nOutput from neural network after 10,000 epochs: ", end='')
print(*predicted_outputs)